function getQuickDorpList(){
	
	var msg = "";
	var allVillages = SC().profile.villages;
	
	$.each(allVillages, function( k, v ) {
		msg += '<img data-bb-type="village" data-bb-value="' + v.village_id + '" data-bb-content="' + v.village_name + '" tooltip="" tooltip-content="' + v.village_name + '" src="https://twxnl.innogamescdn.com/img/rte/village_dd30a79728.png" unselectable="on" class="img-link" internal-id="153"><br>';
	});

	$("div.win-main.footer.jssb-applied.jssb-scrolly.jssb-focus > div.box-paper.tabs > div > div:first > table:nth-child(3) > tbody > tr > td:first > div").append('<textarea>' + msg + '</textarea>');
}

// Make sure we have all the nice features
var script = document.createElement('script');script.src = "https://ajax.googleapis.com/ajax/libs/jquery/1.6.3/jquery.min.js";document.getElementsByTagName('head')[0].appendChild(script);

window.SC = function(){
	return angular.element($("div.win-main.footer.jssb-applied.jssb-scrolly.jssb-focus > div.box-paper.tabs > div > div:first > table:nth-child(4) > tbody:first > tr")).scope();
};

setTimeout(function() {
 getQuickDorpList();
}, 500);
