$(document).ready(function() {
	function php_get(param_to_search_for, page_url) {
		param_send_back = "";
		web_url = page_url;
		delimet_ask = web_url.split("?");
		delimet_and = delimet_ask[1].split("&");
		counter = 0;
		counter_max = delimet_and.length;
		while (counter < counter_max) {
			delimet_equals = delimet_and[counter].split("=");
			if (delimet_equals[0] == param_to_search_for) {
				param_send_back = delimet_equals[1];
				counter = counter_max;
			}
			counter++;
		}
		return param_send_back;
	}

	pathname = window.location.href;
	type_screen = php_get('screen', pathname);

	if (type_screen == "report") {
		/* get the right tables */
		var tabs = document.getElementsByTagName("table");
		var tables = [];
		for (var x = 0; x < tabs.length; x++) {
			if (tabs[x].innerHTML.match(/Aantal:/) && !tabs[x].getElementsByTagName("table")[0]) {
				tables.push(tabs[x]);
			}
		}

		var len, clone, newTr, newTd;
		var unitsWent, unitsLose, unitsSurv;
		var all_units_surv = new Array();
		var aantal_units = 0;
		var external_troop_counter = 1;
		for (var x = 0; x < tables.length; x++) {
			len = tables[0].getElementsByTagName("tr")[1].getElementsByTagName("td").length;
			newTr = tables[0].getElementsByTagName("tr")[1].cloneNode(true);
			newTd = newTr.getElementsByTagName("td")[0].cloneNode(true);
			newTr.innerHTML = "";
			newTd.innerHTML = "Overlevend: ";
			newTr.appendChild(newTd);

			for (var y = 1; y < len; y++) {
				/* calc survived Units */
				unitsWent = tables[x].getElementsByTagName("tr")[1].getElementsByTagName("td")[y].innerHTML;
				unitsLose = tables[x].getElementsByTagName("tr")[2].getElementsByTagName("td")[y].innerHTML;
				unitsSurv = (parseInt(unitsWent) - parseInt(unitsLose));
				
				newTd = newTd.cloneNode(true);
				newTd.innerHTML = unitsSurv;
				newTd.className = (unitsSurv == 0) ? "hidden" : "";
				newTr.appendChild(newTd);
				
				all_units_surv[external_troop_counter] = unitsSurv;
				external_troop_counter++;
				aantal_units++;
			}
			tables[x].appendChild(newTr);
		}
		var new_url_string_part_static = new Array();

		new_url_string_part_static[1] = "asp";
		new_url_string_part_static[2] = "azw";
		new_url_string_part_static[3] = "abi";
		new_url_string_part_static[4] = "asc";
		new_url_string_part_static[5] = "alc";
		new_url_string_part_static[6] = "azc";
		new_url_string_part_static[7] = "ara";
		new_url_string_part_static[8] = "aka";
		new_url_string_part_static[9] = "ari";
		new_url_string_part_static[10] = "asn";
		
		new_url_string_part_static[11] = "dsp";
		new_url_string_part_static[12] = "dzw";
		new_url_string_part_static[13] = "dbi";
		new_url_string_part_static[14] = "dsc";
		new_url_string_part_static[15] = "dlc";
		new_url_string_part_static[16] = "dzc";
		new_url_string_part_static[17] = "dra";
		new_url_string_part_static[18] = "dka";
		new_url_string_part_static[19] = "dri";
		new_url_string_part_static[20] = "dsn";
		new_url_string_part_static[21] = "dpe";
		
		var new_url_last_part = "";
		var int_h = 1;
		var int_t = 1;
		while(int_h < all_units_surv.length){
			new_url_last_part += "&" + new_url_string_part_static[int_t] + "=" + all_units_surv[int_h];
			int_h++;
			int_t++;
			
		}
		var host = window.location.origin;
		var whole_url = host + "/game.php?village=" + php_get("village" , window.location.href) + "&screen=place&mode=sim&script_fill=auto" + new_url_last_part;
		var button_tekst = '<a href="'+whole_url+'" ><br /> » Vul alle gegevens in in de simulator <br /></a>';		
		$("#attack_info_def").after(button_tekst);
		
	} else if (type_screen == "place") {
		var att_speer 	= php_get("asp" , window.location.href);
		var att_zwaard 	= php_get("azw" , window.location.href);
		var att_bijl 	= php_get("abi" , window.location.href);
		var att_scout 	= php_get("asc" , window.location.href);
		var att_lc 		= php_get("alc" , window.location.href);
		var att_zc 		= php_get("azc" , window.location.href);
		var att_ram 	= php_get("ara" , window.location.href);
		var att_kata 	= php_get("aka" , window.location.href);
		var att_ridder 	= php_get("ari" , window.location.href);
		var att_snob 	= php_get("asn" , window.location.href);
		
		var deff_speer 	= php_get("dsp" , window.location.href);
		var deff_zwaard	= php_get("dzw" , window.location.href);
		var deff_bijl 	= php_get("dbi" , window.location.href);
		var deff_scout	= php_get("dsc" , window.location.href);
		var deff_lc 	= php_get("dlc" , window.location.href);
		var deff_zc 	= php_get("dzc" , window.location.href);
		var deff_ram 	= php_get("drm" , window.location.href);
		var deff_kata 	= php_get("dka" , window.location.href);
		var deff_ridder = php_get("dri" , window.location.href);
		var deff_snob 	= php_get("dsn" , window.location.href);
		var deff_peasants 	= php_get("dpe" , window.location.href);
		
		$('input[name=att_spear]').val(att_speer);
		$('input[name=att_sword]').val(att_zwaard);
		$('input[name=att_axe]').val(att_bijl);
		$('input[name=att_spy]').val(att_scout);
		$('input[name=att_light]').val(att_lc);
		$('input[name=att_heavy]').val(att_zc);
		$('input[name=att_ram]').val(att_ram);
		$('input[name=att_catapult]').val(att_kata);
		$('input[name=att_knight]').val(att_ridder);
		$('input[name=att_snob]').val(att_snob);
		$('input[name=def_spear]').val(deff_speer);
		$('input[name=def_sword]').val(deff_zwaard);
		$('input[name=def_axe]').val(deff_bijl);
		$('input[name=def_spy]').val(deff_scout);
		$('input[name=def_light]').val(deff_lc);
		$('input[name=def_heavy]').val(deff_zc);
		$('input[name=def_ram]').val(deff_ram);
		$('input[name=def_catapult]').val(deff_kata);
		$('input[name=def_knight]').val(deff_ridder);
		$('input[name=def_snob]').val(deff_snob);
		$('input[name=def_militia]').val(deff_peasants);
		
	}
});
