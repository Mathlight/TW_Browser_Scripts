﻿$(document).ready(function(){
	function php_get(param_to_search_for, page_url) {
		param_send_back = "";
		web_url = page_url;
		delimet_ask = web_url.split("?");
		delimet_and = delimet_ask[1].split("&");
		counter = 0;
		counter_max = delimet_and.length;
		while (counter < counter_max) {
			delimet_equals = delimet_and[counter].split("=");
			if (delimet_equals[0] == param_to_search_for) {
				param_send_back = delimet_equals[1];
				counter = counter_max;
			}
			counter++;
		}
		return param_send_back;
	}
	
	function getWorld(){
		var worldString = "";
		var worldSplit = pathname.split(".");
		worldString = worldSplit[0];
		worldString = worldString.replace("http://","");
		return worldString;
	}
	
	var pathname = window.location.href;
	var villId = php_get("village",pathname);
	
	var imgUrl = "http://cdn2.tribalwars.net/graphic/reqdef.png";
	var link = '<a href="http://' + getWorld() + '.tribalwars.nl/game.php?village=' + villId + '&screen=reqdef"><span><img src="' + imgUrl + '" /></span></a>';
	if($('#show_incoming_units').length){
		var incomingStuff = $('#show_incoming_units > div > form > table > tbody > .no_ignored_command');
		$( incomingStuff ).each(function( index ) {
			if(this + ' img[src^="http://cdn2.tribalwars.net/graphic/unit/att.png"] '){
				$(this).append(link);
			}
		});
		
	}
});