(function() {
	if(/screen=map/.test(location.href)) {
		return;
	}
	
	var icons_center = 'data:image/png;base64,'+
			'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAABGdBTUEAAK/INwWK6QAAABl0RVh0'+
			'U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAADqSURBVHjaYvz//z8DCBy5++3/z98QNgi8'+
			'/fidIcxchJEBDTCCNIAUc7MzMXCzIuRfffnL8OzNNwxNTHtvfAUrFuZiBgt8BdoCwmI8zAxSIlwM'+
			'Ezdc/4+sgQVEgEz+8fsfRPHPfxCNPxnAmtABE4yBrBhZDEMDyHPoAOT5n1gUw20AeRDmYZhCIaBz'+
			'3gHFsYbSqpNv/gvzc4IVwQBI8ZUbjxhMn3WB+VZZ8xnhGkAAPTRAwJZxP5j+8fQ0w90rRxlip91i'+
			'hGvABhZnqf1X1rFm4JA2hWtiYsADQCaCFIEUo/iBEADZBDMAIMAAitCChPuM2X0AAAAASUVORK5C'+
			'YII=',
		icons_overview = 'data:image/png;base64,'+
			'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAABGdBTUEAAK/INwWK6QAAABl0RVh0'+
			'U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAEYSURBVHjaYvr//z8DKZiFAQpuXtjz/9qe'+
			'FQzfX9xl4JRQZpA2dGAwc45hZEADLOiKn3/8ycDw8RqYDQT/+YUlGNQNXOAaGUHW9KZa/ZfkZ0cx'+
			'CaQRJIZuG/Ovp5f+K/P/RlHIy8ECxiCwfOtJhiN79jAoKYs3SCvpNbIERAQxPHv4nOHu8fVgE0EY'+
			'7CwoEObnYXj78QvDrPZmsBMZb5zf/R/E2rZ8BViB+N+7KLYdufAQxalMIA+BPOwVGcGgqK7O8JJZ'+
			'mUHFMxms+Pr9t3CFmgbmDHZOngxMIE5gyRxGkMaApFJGkCZQyDAJacEVwoC1mwNEA3JcaBoZgp0H'+
			'0ohsupmlJYSDL1ZB/ls/t+s/shg4HkgBAAEGAIt+s+W6POI5AAAAAElFTkSuQmCC',
		icons_troops = 'data:image/png;base64,'+
			'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAABGdBTUEAAK/INwWK6QAAABl0RVh0'+
			'U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAEMSURBVHjaYvz//z8DKYAJmXP/5pn/IIxP'+
			'AwsyZ9XUfjAdll34X1HdhBFmyNQZi8Hi2RmxDCwlhflgE789us5w59EzsMTDp1UMXHKa/2VlZBjk'+
			'FBQYQDSGk4AKGJDZIEViokJwMRibKSzYE8y4dGQvmNazcUYxEaaYg5sP4gczGw+QW//DNCADmCIQ'+
			'4OdmZAD5C+zpL1++MKjISWGY+uPrJ4ZXr9+B+ZFxyYxwP7x89REsGJKaBnbO4ydP4AoxgvXUkR3/'+
			'D29cySAvLcrg5BHC6OQBkQCJ3733FKwZ2U8sx09ehrhX3gDFJJDfzGwYGCzMz/x//foNQsPNY7vB'+
			'DF1tRaxOAHlUUR3BBwgwAEYlXJRjpIvlAAAAAElFTkSuQmCC',
		createBase = function(src, title){
			var a = document.createElement('a'),
				g = document.createElement('img');
			a.style.marginLeft = '3px';
			a.title = title;
			g.src = src;
			g.alt = '';
			g.width = g.height = 12;
			a.appendChild(g);
			return a;
		},
		center = createBase(icons_center, 'Kaart centreren')
		overview = createBase(icons_overview, 'Dorpsoverzicht')
		troops = createBase(icons_troops, 'Troepen sturen'),
		baseURL = '/game.php?',
		RExy = /\((\d+)\|(\d+)\) C\d+$/,
		RExy2 = /^(\d+)\|(\d+)$/,
		REvil = /[?&]village=(\d+)/,
		REid = /[?&]id=(\d+)/;
		REisVil = /[?&]screen=info_village/,
		createButton = function(append, from, href){
			var buttons = from.cloneNode(true);
			buttons.href = baseURL+href;
			append.appendChild(buttons);
		},
		isPlayerProfile = /[?&]screen=info_player/.test(location.href),
		addButtons = function(node, vil, id){
			var coords = node.textContent.match(RExy),
				frag = document.createDocumentFragment(),
				parentNode = node.parentNode,
				nextSibling = node.nextSibling;
			createButton(frag, overview, id+'&screen=overview');
			if(!coords && isPlayerProfile){
				coords = node.parentNode.nextSibling.textContent.match(RExy2);
			}
			if(coords){
				createButton(frag, center, vil+'&screen=map&x='+coords[1]+'&y='+coords[2]);
			}
			createButton(frag, troops, vil+'&screen=place&mode=command&target='+id);
			if(nextSibling) parentNode.insertBefore(frag, nextSibling);
			else parentNode.appendChild(frag);
		},
		nodes = document.links;
	if(nodes.length){
		var i, href, vilmatch, idmatch,
			vv = nodes[0].href.match(/t=\d+/);
		baseURL += vv ? vv+'&village=' : 'village=';
		for(i=0; i<nodes.length; i++){
			if(!REisVil.test(href=nodes[i].href)
			   || !(vilmatch=href.match(REvil)) || !(idmatch=href.match(REid))) continue;
			addButtons(nodes[i], vilmatch[1], idmatch[1]);
		}
	}
})();