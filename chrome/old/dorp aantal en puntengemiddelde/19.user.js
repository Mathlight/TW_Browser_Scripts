(function () {
	var tables = document.getElementsByTagName("table");
	var points_cell;
	var villages_count = -1;
	for (var i=0; i<tables.length; i++) {
		var rows = tables[i].rows;
		if (/\bvis\b/.test(tables[i].className) && rows.length >= 2 && rows[1].cells.length >= 2) {
			if (rows[1].cells[0].innerHTML == "Punten:") {
				points_cell = rows[1].cells[1];
				continue;
			}
			if (!points_cell) continue;
			for (var j=0; j<rows[0].cells.length; j++) {
				var cell_contents = rows[0].cells[j].innerHTML;
				var villages_match = cell_contents.match(/^\s*Dorpen *\((\d+)\)\s*$/);
				if (villages_match) {
					villages_count = villages_match[1];
					break;
				}
				// fallback in case a script modified the contents of the cell
				// less reliable since extra rows could be added
				if (cell_contents == "Punten" || cell_contents == "Co\xF6rdinaten") {
					villages_count = rows.length - 1;
					break;
				}
			}
			if (villages_count > -1) {
				var avg = points_cell.innerHTML.replace(/\D/g, "") / villages_count;
				points_cell.appendChild(document.createTextNode(" (puntengem.: " + Math.round(avg) + ")"));
				break;
			}
		}
	}
})()