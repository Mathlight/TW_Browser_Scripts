﻿$(document).ready(function(){
	
	//	0 = Hoofdgebouw	 ||	1 = Kazerne	||	2 = Stal || 3 = Werkplaats || 4 = Smederij  || 5 = Adelshoeve || 6 = Verzamelplaats || 7 = Standbeeld || 8 = Marktplaats || 9 = Houthakkers || 10 = Leemgroeve || 11 = Ijzermijn || 12 = Boederij || 13 = Opslagplaats || 14 = Schuilplaats || 15 = Muur || 16 = train
	var leURL = new Array();
	leURL[0] =  "main";
	leURL[1] =  "barracks";
	leURL[2] =  "stable";
	leURL[3] =  "garage";
	leURL[4] =  "smith";
	leURL[5] =  "snob";
	leURL[6] =  "place";
	leURL[7] =  "statue";
	leURL[8] =  "market";
	leURL[9] =  "wood";
	leURL[10] =  "stone";
	leURL[11] =  "iron";
	leURL[12] =  "farm";
	leURL[13] =  "storage";
	leURL[14] =  "hide";
	leURL[15] =  "wall";
	leURL[16] =  "train";
	
	var leIMG = new Array();
	leIMG[0] = "http://cdn2.tribalwars.net/graphic/buildings/main.png";
	leIMG[1] = "http://cdn2.tribalwars.net/graphic/buildings/barracks.png";
	leIMG[2] = "http://cdn2.tribalwars.net/graphic/buildings/stable.png";
	leIMG[3] = "http://cdn2.tribalwars.net/graphic/buildings/garage.png";
	leIMG[4] = "http://cdn2.tribalwars.net/graphic/buildings/smith.png";
	leIMG[5] = "http://cdn2.tribalwars.net/graphic/buildings/snob.png";
	leIMG[6] = "http://cdn2.tribalwars.net/graphic/buildings/place.png";
	leIMG[7] = "http://cdn2.tribalwars.net/graphic/buildings/statue.png";
	leIMG[8] = "http://cdn2.tribalwars.net/graphic/buildings/market.png";
	leIMG[9] = "http://cdn2.tribalwars.net/graphic/buildings/wood.png";
	leIMG[10] = "http://cdn2.tribalwars.net/graphic/buildings/stone.png";
	leIMG[11] = "http://cdn2.tribalwars.net/graphic/buildings/iron.png";
	leIMG[12] = "http://cdn2.tribalwars.net/graphic/buildings/farm.png";
	leIMG[13] = "http://cdn2.tribalwars.net/graphic/buildings/storage.png";
	leIMG[14] = "http://cdn2.tribalwars.net/graphic/buildings/hide.png";
	leIMG[15] = "http://cdn2.tribalwars.net/graphic/buildings/wall.png";
	leIMG[16] = "http://cdn2.tribalwars.net/graphic/buildings/barracks.png";
	
	var leText = new Array();
	leText[0] =  "Hoofdgebouw";
	leText[1] =  "Kazerne";
	leText[2] =  "Stal";
	leText[3] =  "Werkplaats";
	leText[4] =  "Smederij";
	leText[5] =  "Adelshoeve";
	leText[6] =  "Verzamelplaats";
	leText[7] =  "Standbeeld";
	leText[8] =  "Marktplaats";
	leText[9] =  "Houthakkers";
	leText[10] =  "Leemgroeve";
	leText[11] =  "Ijzermijn";
	leText[12] =  "Boederij";
	leText[13] =  "Opslagplaats";
	leText[14] =  "Schuilplaats";
	leText[15] =  "Muur";
	leText[16] =  "Massa Rekruteren";
	
	var cookieValues = $.cookie('WPPaTopMenu');
	if(cookieValues == null){
		$.cookie('WPPaTopMenu', '1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1', { expires: 2012, path: '/' });
		cookieValues = $.cookie('WPPaTopMenu');
	}
	var cookieValuesSplit = cookieValues.split(",");
	
	var leShow = new Array();
	leShow[0] =  cookieValuesSplit[0];
	leShow[1] =  cookieValuesSplit[1];
	leShow[2] =  cookieValuesSplit[2];
	leShow[3] =  cookieValuesSplit[3];
	leShow[4] =  cookieValuesSplit[4];
	leShow[5] =  cookieValuesSplit[5];
	leShow[6] =  cookieValuesSplit[6];
	leShow[7] =  cookieValuesSplit[7];
	leShow[8] =  cookieValuesSplit[8];
	leShow[9] =  cookieValuesSplit[9];
	leShow[10] =  cookieValuesSplit[10];
	leShow[11] =  cookieValuesSplit[11];
	leShow[12] =  cookieValuesSplit[12];
	leShow[13] =  cookieValuesSplit[13];
	leShow[14] =  cookieValuesSplit[14];
	leShow[15] =  cookieValuesSplit[15];
	leShow[16] =  cookieValuesSplit[16];
	
	var editIMGLink = "http://scriptsenprogs.nl/media/img/Text-Edit-icon.png";
	
	var pathname = window.location.href;
	function php_get(param_to_search_for, page_url) {
		var param_send_back = "";
		var web_url = page_url;
		var delimet_ask = web_url.split("?");
		var delimet_and = delimet_ask[1].split("&");
		var counter = 0;
		var counter_max = delimet_and.length;
		while (counter < counter_max) {
			var delimet_equals = delimet_and[counter].split("=");
			if (delimet_equals[0] == param_to_search_for) {
				var param_send_back = delimet_equals[1];
				counter = counter_max;
			}
			counter++;
		}
		return param_send_back;
	}
	
	var pathNameSplit = pathname.split(".");
	
	var leURLToStartWith = pathNameSplit[0] + ".tribalwars.nl/game.php?village=" + php_get("village",pathname) + "&screen=";
	
	var lePaMenuString = '<table id="quickbar_outer" align="center" width="100%" cellspacing="0"><tr><td><table id="quickbar_inner" style="border-collapse: collapse;" width="100%"><tr class="topborder"><td class="left"> </td><td class="main"> </td><td class="right"> </td></tr><tr><td class="left"> </td><td class="main"><ul class="menu quickbar">';
	
	var counter = 0;
	var leShowRun1 = leShow;
	$.each(leShowRun1, function(){
		if(this == 1){
			lePaMenuString += '<li ><span><a href="' + leURLToStartWith + leURL[counter] +'"  ><img src="' + leIMG[counter] + '" alt="' + leText[counter] + '" />' + leText[counter] + '</a></span></li>';
		}
		counter++;
	});
	
	lePaMenuString += '<li style="float:right;"><span><a href="#" id="WPPaMenuConfig" ><img src="'+editIMGLink+'" height="18px" /></a></span></li> ';
	lePaMenuString += '</ul></td><td class="right"> </td></tr><tr class="bottomborder"><td class="left"> </td><td class="main"> </td><td class="right"> </td></tr><tr><td class="shadow" colspan="3"><div class="leftshadow"> </div><div class="rightshadow"> </div></td></tr></table></td></tr></table>';
	
	var leConfigEditText = '<table class="main" align="middle" cellpadding="10" id="WPPaTopMenuConfigTable" ><tr><td><b>Ga Naar: </b></td><td><b>Keuze: </b></tr> ';
	
	var counter_2 = 0;
	var leShowRun2 = leShow;
	$.each(leShowRun2, function(){
		if(this == 1){
			leConfigEditText += '<tr><td> <img src="' + leIMG[counter_2] + '" alt="' + leText[counter_2] + '" />' + leText[counter_2] + '<span style="padding-left:20px"></span></td><td>Ja: <input type="radio" name="' + leText[counter_2] + '" value="1" CHECKED /> Nee: <input type="radio" name="' + leText[counter_2] + '" value="0" /> </td></tr>';
		} else{
			leConfigEditText += '<tr><td> <img src="' + leIMG[counter_2] + '" alt="' + leText[counter_2] + '" />' + leText[counter_2] + '<span style="padding-left:20px"></span></td><td>Ja: <input type="radio" name="' + leText[counter_2] + '" value="1" /> Nee: <input type="radio" name="' + leText[counter_2] + '" value="0" CHECKED /> </td></tr>';
		}
		counter_2++;
	});
	
	
	leConfigEditText += '</table><br /><br /><button id="WPPaMenuConfigEditSaveButton"> Sla Op </button>';
	
	$('#WPPaMenuConfigEditSaveButton').live('click', function() {
		var newCookieVar = "";
		var counter_3 = 0;
		var leShowRun3 = leShow;
		$.each(leShowRun3, function(){
			newCookieVar += $('input[name="' + leText[counter_3] + '"]:radio:checked').val();	// ' + leText[counter_3] + '  radio:checked
			newCookieVar += ",";
			counter_3++;
		});
		console.log(newCookieVar);
		$.cookie('WPPaTopMenu', newCookieVar, { expires: 2012, path: '/' });
		location.reload();
	});
	
	$('#WPPaMenuConfig').live('click', function() {
		$("#WPPaMenuConfig").attr("id","WPPaMenuConfigClicked");
	    $("#overviewtable").css("display","none");
		$("#inner-border").before(leConfigEditText);
	});
	
	$('#WPPaMenuConfigClicked').live('click', function() {
		$("#WPPaMenuConfigClicked").attr("id","WPPaMenuConfig");
	    $("#overviewtable").css("display","inline");;
	});

	
	$("#header_info").before(lePaMenuString);
		
});