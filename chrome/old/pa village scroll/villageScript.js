$(document).ready(function() {

	//
	// Global Functions
	//
	function php_get(param_to_search_for, page_url) {
		param_send_back = "";
		web_url = page_url;
		delimet_ask = web_url.split("?");
		delimet_and = delimet_ask[1].split("&");
		counter = 0;
		counter_max = delimet_and.length;
		while (counter < counter_max) {
			delimet_equals = delimet_and[counter].split("=");
			if (delimet_equals[0] == param_to_search_for) {
				param_send_back = delimet_equals[1];
				counter = counter_max;
			}
			counter++;
		}
		return param_send_back;
	}
	
	function getWorld(){
		var worldString = "";
		var worldSplit = pathname.split(".");
		worldString = worldSplit[0];
		worldString = worldString.replace("http://","");
		return worldString;
	}
	
	//
	// Global Variables
	//
	var pathname = window.location.href;
	var type_screen = php_get('screen', pathname);
	var imageReload = "http://icons.iconarchive.com/icons/tatice/cristal-intense/24/Reload-icon.png ";
	var rightArrow = "http://forum.tribalwars.nl/tribalwars/pagination/next-right.png";
	var leftArrow = "http://forum.tribalwars.nl/tribalwars/pagination/previous-right.png";
	var downArrow = "http://i.imgur.com/DbjDh.png";
	var currentVillageId = php_get('village',pathname);
	var dorpNamen = new Array();
	var dorpIds = new Array();
	var allNames = new Array();
	var allIds = new Array();
	var listShown = 0;
	
	//
	// Set stuff on page
	// 
	var previousVillage = '<td style="white-space:nowrap;" class="box-item icon-box nowrap"><img id="previousVillageShow" src="' + leftArrow + '" width="20px"></td>';
	$("#menu_row2_village").before(previousVillage);
	var nextVillage = '<td style="white-space:nowrap;" class="box-item icon-box nowrap"><img id="nextVillageShow" src="' + rightArrow + '" width="20px"></td>';
	$("#menu_row2").append(nextVillage);
	var showListButton = '<td style="white-space:nowrap;" class="box-item icon-box nowrap"><img id="showVillageListNow" src="' + downArrow + '" width="20px" /></td>';
	$("#menu_row2").append(showListButton);
	if(type_screen == "overview_villages"){
		var knopReload = '<td><img id="reloadVillageStorage" src="' + imageReload + '" width="20px" style="vertical-align: middle;" /></td>';
		$("#menu_row2").append(knopReload);
	}
	
	//
	// Workers
	//
	function getAllVillages(){
		var table = document.getElementById("production_table");
		var rows = table.getElementsByTagName("tbody")[0].getElementsByTagName("tr");
		var lengte = rows.length
		var counter = 1;
		while(counter < lengte){
			dorpNamen[counter] = rows[counter].getElementsByTagName("td")[0].innerText;
			dorpIds[counter] = php_get("village",rows[counter].getElementsByTagName("td")[0].getElementsByTagName("a")[0].getAttribute("href"));
			counter++;
		}
		saveAllStuff();
	}
	
	function saveAllStuff(){
		DB_clear();
		var villageCounter = dorpNamen.length;
		var counter = 0;
		while(counter < villageCounter){
			ExtensionData.villages.push({id: dorpIds[counter], name: dorpNamen[counter]});
			DB_save();
			counter++;
		}
	}
	
	function showListWithVillages(){
		var container = '<div id="villageListSelectorHolder" style="float:left;margin-left:45px;border:2px solid #7D510F;"><table class="vis overview_table">';
		var counter1 = 1;
		var rowCount = 0;
		var villageCounter = allNames.length;
		while(counter1 < villageCounter){
			container += '<tr class="nowrap ';
			if(allIds[counter1] == currentVillageId){
				container += 'selected';
			}
			if(rowCount == 0){
				container += 'row_a';
				rowCount++;
			}else{
				container += 'row_b';
				rowCount = 0;
			}
			container += '"><td><a href="';
			var newUrl = pathname.replace(currentVillageId,allIds[counter1]);
			container += newUrl + '">' + allNames[counter1]+'</a></td></tr>';
			counter1++;
		}
		container += '</table></div><div style="clear:both;></div>';
		$("#contentContainer").before(container);
	}
	
	//
	//	Event Handlers:
	//
	$('body').on('click', 'img#reloadVillageStorage', function() {
		console.log('Reload afbeelding geklikt. Nu data ophalen...');
		getAllVillages();
	});
	$('body').on('click', 'img#showVillageListNow', function() {
		if(listShown == 0){
			console.log('Laad de lijst met alle dorpen maar zien...');
			showListWithVillages();
			listShown++;
		}else{
			$("#villageListSelectorHolder").remove();
			listShown = 0;
		}
	});
	$('body').on('click', 'img#previousVillageShow', function() {
		console.log('Vorige afbeelding geklikt. Nu kijken naar welk dorp we gaan...');
		var newVillNum = 0;
		var newVillId = 0;
		var counter = 0;
		var villCount = allIds.length;
		while(counter < villCount){
			if(allIds[counter] == currentVillageId){
				newVillNum = counter;
				counter = villCount;
			}
			counter++;
		}
		if(newVillNum != 0){
			if(newVillNum == 1){
				var newNum = allIds.length - 1;
				newVillId = allIds[newNum];
			}else{
				var newNum = newVillNum - 1;
				newVillId = allIds[newNum];
			}
			var newUrl = pathname.replace(currentVillageId,newVillId);
			window.location = newUrl;
		}
	});
	$('body').on('click', 'img#nextVillageShow', function() {
		console.log('Volgende afbeelding geklikt. Nu kijken naar welk dorp we gaan...');
		var newVillNum = 0;
		var newVillId = 0;
		var counter = 0;
		var villCount = allIds.length;
		while(counter < villCount){
			if(allIds[counter] == currentVillageId){
				newVillNum = counter;
				counter = villCount;
			}
			counter++;
		}
		if(newVillNum != 0){
			if((newVillNum + 1) == allIds.length){
				newVillId = allIds[1];
			}else{
				var newNum = newVillNum + 1;
				newVillId = allIds[newNum];
			}
			var newUrl = pathname.replace(currentVillageId,newVillId);
			window.location = newUrl;
		}
	});
	
	
	// 
	// Storage Handler:
	//
	// Credits goes to 
	// ocanal from Stackoverflow
	// http://stackoverflow.com/a/13943031/1846175
	var village = {
	  id: "1325848", 
	  name : "village1"
	};
	var ExtensionDataName = "twPaVillageScrollData_W" + getWorld();
	var ExtensionData = {
	  dataVersion: 3, //if you want to set a new default data, you must update "dataVersion".
	  villages: []
	};
	
	function DB_setValue(name, value, callback) {
		var obj = {};
		obj[name] = value;
		console.log("Data Saved!");
		chrome.storage.local.set(obj, function() {
			if(callback) callback();
		});
	}

	function DB_load(callback) {
		chrome.storage.local.get(ExtensionDataName, function(r) {
			if (isEmpty(r[ExtensionDataName])) {
				DB_setValue(ExtensionDataName, ExtensionData, callback);
			} else if (r[ExtensionDataName].dataVersion != ExtensionData.dataVersion) {
				DB_setValue(ExtensionDataName, ExtensionData, callback);
			} else {
				ExtensionData = r[ExtensionDataName];
				callback();
			}
		});
	}

	function DB_save(callback) {
		DB_setValue(ExtensionDataName, ExtensionData, function() {
			if(callback) callback();
		});
	}

	function DB_clear() {
		localStorage.clear();
		ExtensionData = {
		  dataVersion: 3,
		  villages: []
		};
	}

	function isEmpty(obj) {
		for(var prop in obj) {
			if(obj.hasOwnProperty(prop))
				return false;
		}
		return true;
	}

	DB_load(function() {
		for (var i = 0; i < ExtensionData.villages.length; i++) {
			allNames[i] = ExtensionData.villages[i].name;
			allIds[i] = ExtensionData.villages[i].id;
		}
	});
});